import numpy as np

# Trace les frontières de decision d'un modèle
def plot_decision_boundaries(model, ax, X, y=None, resolution=100):
    # Assurez-vous que resolution est un entier
    resolution = int(resolution)
    
    # Définir les limites du graphique
    mins = X.min(axis=0) - 1
    maxs = X.max(axis=0) + 1
    
    # Créer une grille de points
    xx, yy = np.meshgrid(np.linspace(mins[0], maxs[0], resolution),
                         np.linspace(mins[1], maxs[1], resolution))
    
    # Prédire les classes pour chaque point de la grille
    Z = model.predict(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)
    
    # Tracer les frontières de décision
    ax.contourf(xx, yy, Z, alpha=0.3)
    
    # Tracer les points de données
    if y is not None:
        ax.scatter(X[:, 0], X[:, 1], c=y, edgecolor='k', marker='o')
    else:
        ax.scatter(X[:, 0], X[:, 1], edgecolor='k', marker='o')
    
    ax.set_xlim(mins[0], maxs[0])
    ax.set_ylim(mins[1], maxs[1])
    ax.set_xticks(())
    ax.set_yticks(())