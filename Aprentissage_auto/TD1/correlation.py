import pandas as pd
import numpy as np

df = pd.read_csv('bankruptcy/data.csv')

# Calculer la matrice de corrélation
correlation_matrix = df.corr()

# Trouver la paire de colonnes avec la corrélation la plus élevée (en valeur absolue)
corr_pairs = correlation_matrix.unstack()
sorted_pairs = corr_pairs.sort_values(kind="quicksort", key=lambda x: abs(x), ascending=False)

# Exclure les corrélations de 1 (corrélation d'une colonne avec elle-même)
sorted_pairs = sorted_pairs[sorted_pairs < 1]

# Afficher la meilleure corrélation
best_corr = sorted_pairs.idxmax()
best_corr_value = sorted_pairs.max()

# Obtenir les indices des colonnes
col1_index = df.columns.get_loc(best_corr[0])
col2_index = df.columns.get_loc(best_corr[1])

print(f"La meilleure corrélation est entre {best_corr[0]} (colonne {col1_index}) et {best_corr[1]} (colonne {col2_index}) avec une valeur de {best_corr_value}")