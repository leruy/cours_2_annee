# Importation des librairies
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import accuracy_score, classification_report
from sklearn.decomposition import PCA
from function import plot_decision_boundaries

# Chargement des données
data = pd.read_csv("bankruptcy/data.csv")

# X = caractéristiques des individus pour la prédiction de la banqueroute
# y = variable à prédire
X = data.drop(columns=["Bankrupt?"])
y = data["Bankrupt?"]

# Normalisation des caractéristiques
scaler = StandardScaler()
X_scaled = scaler.fit_transform(X)

# Réduction des dimensions à 2 pour la visualisation
pca = PCA(n_components=2)
X_pca = pca.fit_transform(X_scaled)

# Division des données en ensembles d'entraînement et de test avec stratification
X_train, X_test, y_train, y_test = train_test_split(X_pca, y, test_size=0.2, random_state=42, stratify=y)

# Entraînement du modèle KNN avec les données réduites
model = KNeighborsClassifier(n_neighbors=3)
model.fit(X_train, y_train)

# Visualisation
fig, ax = plt.subplots(1, 3, figsize=(15, 6))
ax[0].scatter(X_train[:, 0], X_train[:, 1], c='b', label="Entraînement")
ax[0].scatter(X_test[:, 0], X_test[:, 1], c='r', label="Test")
ax[0].legend(loc='best')
ax[0].axis('off')

plot_decision_boundaries(model, ax[1], X_train, y_train)
ax[1].set_title("Base d'entraînement")

y_pred = model.predict(X_test)
accuracy = accuracy_score(y_test, y_pred) * 100
plot_decision_boundaries(model, ax[2], X_test, y_test)
ax[2].set_title("Base de test. Précision : %.2f%%" % accuracy)

plt.show()
