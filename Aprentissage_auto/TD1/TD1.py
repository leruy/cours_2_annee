# Importation des librairies
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score, classification_report
from function import plot_decision_boundaries

# Chargement des données
data = pd.read_csv("bankruptcy/data.csv")

# X = caractéristiques des individus pour la prédiction de la banqueroute
# y = variable à prédire
X = data.drop(columns=["Bankrupt?"])
y = data["Bankrupt?"]

# Utilisation des deux premières caractéristiques pour l'entraînement et la visualisation
X = X.iloc[4, 89]

# Division des données en ensembles d'entraînement et de test
# test_size = 0.2 signifie que 20% des données sont utilisées pour tester le modèle
# random_state = nombre pour générer l'aléatoir pour la division des données, on peut reproduire avec le meme nombre
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=40)

# Entraînement du modèle avec les deux premières caractéristiques
model = KNeighborsClassifier(n_neighbors=3)
model.fit(X_train, y_train)

# Visualisation
fig, ax = plt.subplots(1, 3, figsize=(15, 6))
ax[0].scatter(X_train.iloc[:, 0], X_train.iloc[:, 1], c='b', label="Entraînement")
ax[0].scatter(X_test.iloc[:, 0], X_test.iloc[:, 1], c='r', label="Test")
ax[0].legend(loc='best')
ax[0].axis('off')

plot_decision_boundaries(model, ax[1], X_train.values, y_train.values)
ax[1].set_title("Base d'entraînement")

y_pred = model.predict(X_test)
num_correct_predictions = (y_pred == y_test).sum()
accuracy = (num_correct_predictions / y_test.shape[0]) * 100
plot_decision_boundaries(model, ax[2], X_test.values, y_test.values)
ax[2].set_title("Base de test. Précision : %.2f%%" % accuracy)

plt.show()